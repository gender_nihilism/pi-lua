--pi/4 = 1 -1/3 + 1/5 - 1/7 + 1/9 and so on
--this program calculates pi using Leibniz's Formula
--and prints it one step at a time, once a second
--to print it as fast as your processor runs, comment the os.execute line
--it will take about 11.6 minutes to consistently reach 3.14, and it'll take
--longer and longer to get more correct until eventually you'll be waiting
--a comically long time to reach the next correct digit
local number = 1 
local result = 1 
local executed = false

while true do

	number = number + 2 				

	if executed then	
		result = result + 1/number 
		executed = false 
	else
		result = result - 1/number
		executed = true		
	end

	local pi = result * 4		
	print(pi)
	os.execute ("sleep 1")	

end

